from django.shortcuts import render, redirect
from lab_2.models import Note
from .forms import NoteForm



def index(request):
    notePBP = Note.objects.all() 
    response = {'Note': notePBP}  
    return render(request, 'lab4_index.html', response)

def add_note(request) :
    form = NoteForm(request.POST or None)
    if(form.is_valid and request.method == 'POST'):
        form.save()
        redirect('/lab-4')
    response = {'form': form}
    return render(request, 'lab4_form.html', response)

def note_list(request):
    notePBP = Note.objects.all() 
    response = {'Note': notePBP}  
    return render(request, 'lab4_note_list.html', response)


