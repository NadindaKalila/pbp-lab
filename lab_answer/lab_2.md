Apakah perbedaan antara JSON dan XML?

JSON :
- Bukan merupakan bahasa, melainkan format yang ada di dalam JavaScript yang digunakan untuk menyimpan data
- JSON mudah dipahami atau dibaca manusia
- Data yang disimpan berbentuk seperti map, yaitu memiliki key dan value
- Penggunaan memori sedikit, sehingga lebih cepat
- Tipe data di dalamnya terbatas (String, bilangan, boolean, object primitif)

XML : 
- Merupakan bahasa markup, yang digunakan untuk menyimpan data. 
- Memiliki kemiripkan dengan HTML, yaitu menggunakan tag untuk setiap elemennya
- Data yang disimpan berbentuk tree structure
- Penggunaan memori lebih banyak
- Tipe data lebih luas seperti tabel, charts, dam tipe data non-primitif

sumber : 
https://www.monitorteknologi.com/perbedaan-json-dan-xml/



Apakah perbedaan antara HTML dan XML?

HTML :
- Merupakan bahasa markup yang digunakan untuk mengatur tampilan data
- Tidak bersifat case sensitive
- Menggunakan tag untuk pembuatan elemen, dan tag yang dapat digukanan terbatas atau sudah ditentukan
- Terdapat beberapa elemen yang tidak diharuskan menggunakan end tag (disebut sebagai empty element)

XML :
- Merupakan bahasa markup yang yang digunakan untuk menyimpan data
- Bersifat case sensitive
- Mirip dengan HTML, yaitu menggunakan tag untuk pembuatan elemen, namun, tag yang digunakan dapat ditentukan oleh pengguna
- Diharuskan adanya end tag pada setiap elemen

sumber :
https://blogs.masterweb.com/perbedaan-xml-dan-html/
