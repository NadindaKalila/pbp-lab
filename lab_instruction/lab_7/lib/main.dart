import 'package:flutter/material.dart';
import 'package:cool_alert/cool_alert.dart';

void main() {
  runApp(MaterialApp(
    title: "Joeurnnal",
    theme: ThemeData(
      primarySwatch: Colors.deepPurple,
    ),
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();
  DateTime date = DateTime.now();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black54,
        title: Text("Joeurnnal 좋은날"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      labelText: "Task",
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    onTap: (){
                      showDatePicker(
                        context: context, 
                        initialDate: date, 
                        firstDate: DateTime(2021), 
                        lastDate: DateTime(2050),
                      );
                    },
                    decoration: new InputDecoration(
                      labelText: "Deadline",
                      hintText: (date.toString()),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                  ),
                ),
                RaisedButton(onPressed: (){
                  CoolAlert.show(
                    context: context,
                    type: CoolAlertType.success,
                    text: "New task added successfully. Move to the list page",
                  );
                }, 
                elevation: 10.0,
                color: Color(0xFF7862AE),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                child: Text('Add', style: TextStyle(color: Colors.white),),           
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}



//percobaan

//Future buat representasi fungsi yang punya potensi untuk menghasilkan error di masa depan
  // Future<Null> _selectDate(BuildContext context) async{ //memungkinkan bekerja sembari nunggu yang lain lagi loading
  //   DateTime _datePicker = await showDatePicker(
  //     context: context, 
  //     initialDate: _date, 
  //     firstDate: DateTime(2021), 
  //     lastDate: DateTime(2050),
  //   );

  //   if(_datePicker != null && _datePicker != _date){
  //     setState((){
  //       _date = _datePicker;
  //     });
  //   }
  // }

  // double nilaiSlider = 1;
  // bool nilaiCheckBox = false;
  // bool nilaiSwitch = true;
  // DateTime _date = DateTime.now();


// icon: Icon(Icons.people),
  // validator: (value) {
                    //   if (value.isEmpty) {
                    //     return 'Nama tidak boleh kosong';
                    //   }
                    //   return null;
                    // },

  //   // if (_formKey.currentState.validate()) {}
                  // }, //New task added successfully. Move to the list page
                // ),
