import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Joeurnal',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black54,
          title: const Text('Joeurnnal 좋은날'),
        ),
        body: Container(
          child: ListView(children: <Widget>[
              Image(
                image: AssetImage('assets/todo.png'),
              ),
              Card(
              elevation: 10.0,
              color : Color(0xFF7862AE),
              child: Container(
                padding: EdgeInsets.all(10.0),
                height: 73,
                child : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Tugas PBP',
                      style: TextStyle(fontSize: 20.0,color: Colors.white,fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      'Deadline: 15 November 2021',
                      style: TextStyle(fontSize: 15.0,color: Colors.white),
                    )
                  ],
                ),
              ),
            ), //card
            Card(
              elevation: 10.0,
              color : Color(0xFF7862AE),
              child: Container(
                padding: EdgeInsets.all(10.0),
                height: 73,
                child : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Tugas MPPI',
                      style: TextStyle(fontSize: 20.0,color: Colors.white,fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      'Deadline: 27 November 2021',
                      style: TextStyle(fontSize: 15.0,color: Colors.white),
                    )
                  ],
                ),
              ),
            ), //card
            Column(
              children: [
                SizedBox(height: 7.0),
                RaisedButton(onPressed: (){
                  print("RaisedButton");
                }, 
                elevation: 10.0,
                color: Color(0xFF7862AE),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                child: Text('Add New Task', style: TextStyle(color: Colors.white),),           
                ),
              ],
            ),
          ],),
 
        ),
      ),
    );
  }
}







// import 'package:flutter/material.dart';

// import './dummy_data.dart';
// import './screens/tabs_screen.dart';
// import './screens/meal_detail_screen.dart';
// import './screens/category_meals_screen.dart';
// import './screens/filters_screen.dart';
// import './screens/categories_screen.dart';
// import './models/meal.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> {
//   Map<String, bool> _filters = {
//     'gluten': false,
//     'lactose': false,
//     'vegan': false,
//     'vegetarian': false,
//   };
//   List<Meal> _availableMeals = DUMMY_MEALS;
//   List<Meal> _favoriteMeals = [];

//   void _setFilters(Map<String, bool> filterData) {
//     setState(() {
//       _filters = filterData;

//       _availableMeals = DUMMY_MEALS.where((meal) {
//         if (_filters['gluten'] && !meal.isGlutenFree) {
//           return false;
//         }
//         if (_filters['lactose'] && !meal.isLactoseFree) {
//           return false;
//         }
//         if (_filters['vegan'] && !meal.isVegan) {
//           return false;
//         }
//         if (_filters['vegetarian'] && !meal.isVegetarian) {
//           return false;
//         }
//         return true;
//       }).toList();
//     });
//   }

//   void _toggleFavorite(String mealId) {
//     final existingIndex =
//         _favoriteMeals.indexWhere((meal) => meal.id == mealId);
//     if (existingIndex >= 0) {
//       setState(() {
//         _favoriteMeals.removeAt(existingIndex);
//       });
//     } else {
//       setState(() {
//         _favoriteMeals.add(
//           DUMMY_MEALS.firstWhere((meal) => meal.id == mealId),
//         );
//       });
//     }
//   }

//   bool _isMealFavorite(String id) {
//     return _favoriteMeals.any((meal) => meal.id == id);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Daftar Makanan',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//         accentColor: Colors.indigo,
//         canvasColor: Color.fromRGBO(255, 254, 229, 1),
//         fontFamily: 'Raleway',
//         textTheme: ThemeData.light().textTheme.copyWith(
//             bodyText1: TextStyle(
//               color: Color.fromRGBO(20, 51, 51, 1),
//             ),
//             bodyText2: TextStyle(
//               color: Color.fromRGBO(20, 51, 51, 1),
//             ),
//             headline6: TextStyle(
//               fontSize: 20,
//               fontFamily: 'RobotoCondensed',
//               fontWeight: FontWeight.bold,
//             )),
//       ),
//       // home: CategoriesScreen(),
//       initialRoute: '/', // default is '/'
//       routes: {
//         '/': (ctx) => TabsScreen(_favoriteMeals),
//         CategoryMealsScreen.routeName: (ctx) =>
//             CategoryMealsScreen(_availableMeals),
//         MealDetailScreen.routeName: (ctx) => MealDetailScreen(_toggleFavorite, _isMealFavorite),
//         FiltersScreen.routeName: (ctx) => FiltersScreen(_filters, _setFilters),
//       },
//       onGenerateRoute: (settings) {
//         print(settings.arguments);
//       },
//       onUnknownRoute: (settings) {
//         return MaterialPageRoute(
//           builder: (ctx) => CategoriesScreen(),
//         );
//       },
//     );
//   }
// }



